#include "opencv2\core\core.hpp"
#include "opencv2\highgui\highgui.hpp"
#include "opencv2\imgproc\imgproc.hpp"
using namespace cv;
#include <iostream>
using namespace std;

void thresFunction(int x,void* data)
{
	*(int*)data = x;
}

typedef struct PointEdge
{
	int x;
	int y;
}POINTEDGE;

POINTEDGE p_TL={0},p_TR={0},p_BR={0},p_BL={0};
int firstTime = 1;

void thresFunction1(int x,void* data)
{
	*(double*)data = (double)x;
}

void detectEdges(int x,int y)
{
	
}

int main(int argc,char** argv)
{

	VideoCapture cam(0);
	CV_Assert(cam.isOpened());
	Mat frame;

	cam.set(CV_CAP_PROP_FRAME_WIDTH,800);
	cam.set(CV_CAP_PROP_FRAME_HEIGHT,448);
	
	do
	{
		cam >> frame;
	}while(frame.empty());
	namedWindow("Keyboard");namedWindow("Ref_Thres");namedWindow("Finger_Thres");

	int RLg=112,RHg=182,GLg=243,GHg=255,BLg=182,BHg=255;

	int RLf=112,RHf=182,GLf=243,GHf=255,BLf=182,BHf=255;

	createTrackbar("RHg","Ref_Thres",&RHg,255,thresFunction,&RHg);
	createTrackbar("RLg","Ref_Thres",&RLg,255,thresFunction,&RLg);
	createTrackbar("GHg","Ref_Thres",&GHg,255,thresFunction,&GHg);
	createTrackbar("GLg","Ref_Thres",&GLg,255,thresFunction,&GLg);
	createTrackbar("BHg","Ref_Thres",&BHg,255,thresFunction,&BHg);
	createTrackbar("BLg","Ref_Thres",&BLg,255,thresFunction,&BLg);


	createTrackbar("RHf","Finger_Thres",&RHf,255,thresFunction,&RHf);
	createTrackbar("RLf","Finger_Thres",&RLf,255,thresFunction,&RLf);
	createTrackbar("GHf","Finger_Thres",&GHf,255,thresFunction,&GHf);
	createTrackbar("GLf","Finger_Thres",&GLf,255,thresFunction,&GLf);
	createTrackbar("BHf","Finger_Thres",&BHf,255,thresFunction,&BHf);
	createTrackbar("BLf","Finger_Thres",&BLf,255,thresFunction,&BLf);


	vector<vector<Point>> contsBorder,contsFinger;
	vector<Vec4i> hierarchy;

	double cannyThresh = 0;

	namedWindow("Extra");

	
	createTrackbar("FirstTime","Extra",&firstTime,2,thresFunction,&firstTime);

	createTrackbar("canny","Keyboard",NULL,300,thresFunction1,&cannyThresh);
	Rect rec;
	Mat frameBorder,frameFingers;

	int virtualWidth=0,virtualHeight=0,i=0;

	while(true)
	{
		cam >> frame;
		GaussianBlur(frame,frame,Size(5,5),1);
		//cvtColor(frame,frame,CV_BGR2GRAY,THRESH_BINARY);
		//equalizeHist( frame,frame );
	if(firstTime)
		{
		inRange(frame,Scalar(CV_RGB(RLg,GLg,BLg)),Scalar(CV_RGB(RHg,GHg,BHg)),frameBorder);
		//dilate(frameBorder,frameBorder,getStructuringElement(MORPH_ELLIPSE,Size(4,4)));
		//Canny(frameBorder,frameBorder,cannyThresh,3*cannyThresh);
		findContours(frameBorder,contsBorder,hierarchy,CV_RETR_EXTERNAL,CV_CHAIN_APPROX_SIMPLE);
		imshow("Keyboard",frameBorder);
			waitKey(10);
		if((contsBorder.size()<3) || (contsBorder.size()>4))
		{	
			cout << "ALign Keyboard" << endl;
			continue;
		}
		else
			cout<< "Keyboard OK" <<endl;
	
			for (i=0;i<contsBorder.size();i++)
			{
				drawContours( frameBorder, contsBorder,i, Scalar(255), 2, 8, hierarchy,0, Point() );
				rec = boundingRect(contsBorder[i]);
				detectEdges(rec.height/2,rec.width/2);
				rectangle(frameBorder,rec,Scalar(255));
			}
			imshow("Keyboard",frameBorder);
			waitKey(10);

			continue;
		}

	  inRange(frame,Scalar(CV_RGB(RLg,GLg,BLg)),Scalar(CV_RGB(RHg,GHg,BHg)),frameFingers);
		dilate(frameFingers,frameFingers,getStructuringElement(MORPH_ELLIPSE,Size(4,4)));
		Canny(frameFingers,frameFingers,cannyThresh,3*cannyThresh);
		findContours(frameFingers,contsFinger,hierarchy,CV_RETR_EXTERNAL,CV_CHAIN_APPROX_SIMPLE);


		for (i=0;i<contsFinger.size();i++)
			{
				drawContours( frameFingers, contsFinger,i, Scalar(255), 2, 8, hierarchy,0, Point() );
				rec = boundingRect(contsFinger[i]);
				detectEdges(rec.height/2,rec.width/2);
				rectangle(frameFingers,rec,Scalar(255));
			}


		virtualWidth = abs(p_TR.x - p_TL.x);
		virtualHeight = abs(p_TL.y - p_BL.y);

		
	}

	return 0;
}