#include "opencv2\core\core.hpp"
#include "opencv2\highgui\highgui.hpp"
#include "opencv2\imgproc\imgproc.hpp"
using namespace cv;
#include <iostream>
using namespace std;

namespace
{
	
	Mat frame;
	vector<Vec4i> hierarchy;

	double cannyThresh = 0;
	Rect rec;
	Mat frameBorder,frameFingers;
	int i=0;
	int firstTime = 1;
	float virtualWidth=0,virtualHeight=0;
	vector<vector<Point>> contsBorder,contsFinger;

	int RLg=248,RHg=255,GLg=211,GHg=254,BLg=42,BHg=255;
	int RLf=231,RHf=255,GLf=0,GHf=169,BLf=0,BHf=243;

}
void thresFunction(int x,void* data)
{
	*(int*)data = x;
}

/*typedef struct PointEdge
{
	int x;
	int y;
}POINTEDGE;
*/
class POINTEDGE
{
public:
	int x;
	int y;
	long long int dist()
	{
		return ((x*x)+(y*y));
	}
};

POINTEDGE cornerPoints[4];
/*
0 -> TopLeft
1 -> TopRight
2 -> BottomLeft
3 -> BottomRight
*/


void thresFunction1(int x,void* data)
{
	*(double*)data = (double)x;
}

void createBars()
{
	createTrackbar("RHg","Ref_Thres",&RHg,255,thresFunction,&RHg);
	createTrackbar("RLg","Ref_Thres",&RLg,255,thresFunction,&RLg);
	createTrackbar("GHg","Ref_Thres",&GHg,255,thresFunction,&GHg);
	createTrackbar("GLg","Ref_Thres",&GLg,255,thresFunction,&GLg);
	createTrackbar("BHg","Ref_Thres",&BHg,255,thresFunction,&BHg);
	createTrackbar("BLg","Ref_Thres",&BLg,255,thresFunction,&BLg);


	createTrackbar("RHf","Finger_Thres",&RHf,255,thresFunction,&RHf);
	createTrackbar("RLf","Finger_Thres",&RLf,255,thresFunction,&RLf);
	createTrackbar("GHf","Finger_Thres",&GHf,255,thresFunction,&GHf);
	createTrackbar("GLf","Finger_Thres",&GLf,255,thresFunction,&GLf);
	createTrackbar("BHf","Finger_Thres",&BHf,255,thresFunction,&BHf);
	createTrackbar("BLf","Finger_Thres",&BLf,255,thresFunction,&BLf);

	
	createTrackbar("FirstTime","Extra",&firstTime,1,thresFunction,&firstTime);

	createTrackbar("canny","Keyboard",NULL,300,thresFunction1,&cannyThresh);

}

void detectEdges(float x,float y)
{
	float ah,bh,ch,av,bv,cv,disth,distv;

	ah=cornerPoints[1].x-cornerPoints[0].x;
	bh=cornerPoints[0].y-cornerPoints[1].y;
	ch=(((cornerPoints[1].y-cornerPoints[0].y)*(cornerPoints[0].x))-((cornerPoints[1].x-cornerPoints[0].x)*(cornerPoints[0].y)));
	disth=(abs(ah*x+bh*y+ch))/(float)(sqrt(ah*ah+bh*bh));

	av=cornerPoints[2].x-cornerPoints[0].x;
	bv=cornerPoints[0].y-cornerPoints[2].y;
	cv=(((cornerPoints[2].y-cornerPoints[0].y)*(cornerPoints[0].x))-((cornerPoints[2].x-cornerPoints[0].x)*(cornerPoints[0].y)));
	distv=(abs(av*x+bv*y+cv))/(float)(sqrt(av*av+bv*bv));

	//cout<< disth << " " <<distv<<endl;
}

void swap(int* x,int* y)
{
	int temp;
	temp = *x;
	*x = *y;
	*y = temp;
}

void checkCornerPoints(POINTEDGE* p)
{
	POINTEDGE temp;
	for (int c = 0 ; c < ( 4 - 1 ); c++)
  {
    for (int d = 0 ; d < 4 - c - 1; d++)
    {
      if (p[d].dist() > p[d+1].dist())
      {
        temp   = p[d];
        p[d]   = p[d+1];
        p[d+1] = temp;
      }
    }
  }

	virtualWidth=sqrt(((p[0].x-p[2].x)*(p[0].x-p[2].x))+((p[0].y-p[2].y)*(p[0].y-p[2].y)));
	virtualHeight=sqrt(((p[0].x-p[1].x)*(p[0].x-p[1].x))+((p[0].y-p[1].y)*(p[0].y-p[1].y)));

}


int main(int argc,char** argv)
{
	VideoCapture cam(0);
	CV_Assert(cam.isOpened());
	cam.set(CV_CAP_PROP_FRAME_WIDTH,800);
	cam.set(CV_CAP_PROP_FRAME_HEIGHT,448);
	
	do
	{
		cam >> frame;
	}while(frame.empty());

	namedWindow("Keyboard");namedWindow("Ref_Thres");namedWindow("Finger_Thres");
	namedWindow("Extra");namedWindow("Fingering");
	createBars();

	while(true)
	{
		cam >> frame;
		GaussianBlur(frame,frame,Size(5,5),1);
		//cvtColor(frame,frame,CV_BGR2GRAY,THRESH_BINARY);
		//equalizeHist( frame,frame );
		if(firstTime)
		{
			inRange(frame,Scalar(CV_RGB(RLg,GLg,BLg)),Scalar(CV_RGB(RHg,GHg,BHg)),frameBorder);
			morphologyEx(frameBorder,frameBorder,MORPH_CLOSE,getStructuringElement(MORPH_ELLIPSE,Size(5,5)));
			dilate(frameBorder,frameBorder,getStructuringElement(MORPH_ELLIPSE,Size(5,5)));
			findContours(frameBorder,contsBorder,hierarchy,CV_RETR_EXTERNAL ,CV_CHAIN_APPROX_NONE );
			imshow("Keyboard",frameBorder);
				waitKey(10);
			if(contsBorder.size()!=4)
			{	
				cout << "ALign Keyboard " << contsBorder.size() <<  endl;
				continue;
			}
			else
				cout<< "Keyboard OK" << " " << contsBorder.size() << endl;
	
			for (i=0;i<contsBorder.size();i++)
			{
				drawContours( frameBorder, contsBorder,i, Scalar(255), 2, 8, hierarchy,0, Point() );
				rec = boundingRect(contsBorder[i]);
				rectangle(frameBorder,rec,Scalar(255));
				cornerPoints[i].x = rec.x;
				cornerPoints[i].y = rec.y;
			}

			checkCornerPoints(cornerPoints);
			imshow("Keyboard",frameBorder);
			waitKey(10);
			continue;
		}

		inRange(frame,Scalar(CV_RGB(RLf,GLf,BLf)),Scalar(CV_RGB(RHf,GHf,BHf)),frameFingers);
		morphologyEx(frameFingers,frameFingers,MORPH_CLOSE,getStructuringElement(MORPH_ELLIPSE,Size(5,5)));
		dilate(frameFingers,frameFingers,getStructuringElement(MORPH_ELLIPSE,Size(5,5)));
		Canny(frameFingers,frameFingers,cannyThresh,3*cannyThresh);
		findContours(frameFingers,contsFinger,hierarchy,CV_RETR_EXTERNAL,CV_CHAIN_APPROX_NONE );

		for (i=0;i<contsFinger.size();i++)
		{
			drawContours( frameFingers, contsFinger,i, Scalar(255), 2, 8, hierarchy,0, Point() );
			rec = boundingRect(contsFinger[i]);
			detectEdges(rec.height/2.0,rec.width/2.0);
			cout << "Finger Area : " << rec.area() << endl;
			//cout<< "Finger Pos: " << rec.height/2.0<<" "<<rec.width/2.0<<endl;
			rectangle(frameFingers,rec,Scalar(255));
		}

		imshow("Fingering",frameFingers);
		waitKey(10);
		
	}

	return 0;
}