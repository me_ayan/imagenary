#include <iostream>
using namespace std;
#include "opencv2\core\core.hpp"
#include "opencv2\highgui\highgui.hpp"
#include "opencv2\imgproc\imgproc.hpp"
using namespace cv;
#include<Windows.h>

namespace
{
	POINT t1;
	VideoCapture cam(0);
	Moments momentR,momentB_L,momentB_R;
	Mat frame,frameB,frameR,frameB_L,frameB_R;
	double curAreaB_L,curAreaB_R,lastAreaB_L=0,lastAreaB_R=0;
	double xmeanCurR,ymeanCurR,xmeanLastR=0,ymeanLastR=0;
	int RLr=101,RHr=255,GLr=0,GHr=66,BLr=0,BHr=73;
	int RLb=0,RHb=134,GLb=201,GHb=255,BLb=105,BHb=220;
	int thresh=2,clickThreshLeft=300,clickThreshRight=300;
	bool nomove=true,clickon=true;
	int mouseon=0;
	int screenHeightO,screenWidthO;
	int screenHeight=0,screenWidth=0;
	int camWidth=752,camHeight=416;
	int factor=16;
}

void thresFunction(int x,void* data)
{
	*(int*)data = x;
}

void createTrackBars()
{
	createTrackbar("pxThresh","Adjustments",&thresh,255,thresFunction,&thresh);
	createTrackbar("LeftClickSizeThresh","Adjustments",&clickThreshLeft,2000,thresFunction,&clickThreshLeft);
	createTrackbar("RightClickSizeThresh","Adjustments",&clickThreshRight,2000,thresFunction,&clickThreshRight);
	createTrackbar("Smooth Adjust","Adjustments",&factor,20,thresFunction,&factor);
	createTrackbar("MouseMove","Adjustments",&mouseon,100,thresFunction,&mouseon);

	createTrackbar("R_HIGHr","RedThreshold",&RHr,255,thresFunction,&RHr);
	createTrackbar("R_LOWr","RedThreshold",&RLr,255,thresFunction,&RLr);
	createTrackbar("G_HIGHr","RedThreshold",&GHr,255,thresFunction,&GHr);
	createTrackbar("G_LOWr","RedThreshold",&GLr,255,thresFunction,&GLr);
	createTrackbar("B_HIGHr","RedThreshold",&BHr,255,thresFunction,&BHr);
	createTrackbar("B_LOWr","RedThreshold",&BLr,255,thresFunction,&BLr);


	createTrackbar("R_HIGHb","BlueThreshold",&RHb,255,thresFunction,&RHb);
	createTrackbar("R_LOWb","BlueThreshold",&RLb,255,thresFunction,&RLb);
	createTrackbar("G_HIGHb","BlueThreshold",&GHb,255,thresFunction,&GHb);
	createTrackbar("G_LOWb","BlueThreshold",&GLb,255,thresFunction,&GLb);
	createTrackbar("B_HIGHb","BlueThreshold",&BHb,255,thresFunction,&BHb);
	createTrackbar("B_LOWb","BlueThreshold",&BLb,255,thresFunction,&BLb);

}

void click (char s)
{  
  INPUT Input={0};
  Input.type= INPUT_MOUSE;
  (s=='l')?(Input.mi.dwFlags = MOUSEEVENTF_LEFTDOWN):(Input.mi.dwFlags = MOUSEEVENTF_RIGHTDOWN);
  ::SendInput(1,&Input,sizeof(INPUT));
  ::ZeroMemory(&Input,sizeof(INPUT));
  Input.type      = INPUT_MOUSE;
  (s=='l')?(Input.mi.dwFlags = MOUSEEVENTF_LEFTUP):(Input.mi.dwFlags = MOUSEEVENTF_RIGHTUP);
  ::SendInput(1,&Input,sizeof(INPUT));
}


int detectClickLeft(double curArea, double lastArea)
{
	if(mouseon && (curArea-lastArea)>clickThreshLeft)
			return 1;
	cout<< "No LClick" <<endl;
	return 0;

}

int detectClickRight(double curArea, double lastArea)
{
	if(mouseon && (curArea-lastArea)>clickThreshRight)
			return 1;
	cout<< "No RClick" <<endl;
	return 0;

}

void checkmotion(double curValX,double curValY, double lastValX,double lastValY)
{
	if(mouseon && clickon)
	{
		if(abs(curValX-lastValX)>thresh)
			SetCursorPos((int)((curValX/camWidth)*screenWidth),(int)((curValY/camHeight)*screenHeight));
		if(abs(curValY-lastValY)>thresh)
			SetCursorPos((int)((curValX/camWidth)*screenWidth),(int)((curValY/camHeight)*screenHeight));
	}
}

int main(int argc, char** argv)
{   
	namedWindow("WinR"); namedWindow("WinB"); namedWindow("RedThreshold"); namedWindow("BlueThreshold");namedWindow("Adjustments");

	screenHeightO=GetSystemMetrics(SM_CYSCREEN);
	screenWidthO=GetSystemMetrics(SM_CXSCREEN);

	CV_Assert(cam.isOpened());
	
	cam.set(CV_CAP_PROP_FRAME_WIDTH,camWidth);
	cam.set(CV_CAP_PROP_FRAME_HEIGHT,camHeight);

	createTrackBars();

	do
	{cam.read(frame);
	}while(frame.empty());
	
	while(true)
	{
		cam.read(frame);

		GaussianBlur(frame,frame,Size(11,11),2);
		inRange(frame,Scalar(CV_RGB(RLr,GLr,BLr)),Scalar(CV_RGB(RHr,GHr,BHr)),frameR);
		inRange(frame,Scalar(CV_RGB(RLb,GLb,BLb)),Scalar(CV_RGB(RHb,GHb,BHb)),frameB);
		dilate(frameR,frameR,getStructuringElement(MORPH_ELLIPSE,Size(4,4)));
		dilate(frameB,frameB,getStructuringElement(MORPH_ELLIPSE,Size(4,4)));
				
		momentR = moments(frameR,true);
		if(!momentR.m00)
			momentR.m00 = 1;
		xmeanCurR=momentR.m10/momentR.m00;
		ymeanCurR=momentR.m01/momentR.m00;
		if(xmeanCurR<=0)
			xmeanCurR = frameR.cols/2;

		frameB_L = frameB(Range::all(),Range(0,(int)xmeanCurR));
		frameB_R = frameB(Range::all(),Range((int)xmeanCurR,frameR.cols));

		momentB_L = moments(frameB_L,true);
		momentB_R = moments(frameB_R,true);
		curAreaB_L = momentB_L.m00;
		curAreaB_R = momentB_R.m00;

		screenHeight=(int)(screenHeightO * (factor/10.0));
		screenWidth=(int)(screenWidthO * (factor/10.0));

		if(detectClickLeft(curAreaB_L,lastAreaB_L))
			{
				cout << "LeftClick" << endl;
				clickon=false;
				click('l');
				clickon=true;

			}

		if(detectClickRight(curAreaB_R,lastAreaB_R))
			{
				cout << "RightClick" << endl;
				clickon=false;
				click('r');
				clickon=true;
			}
		
		checkmotion(xmeanCurR,ymeanCurR,xmeanLastR,ymeanLastR);

		xmeanLastR=xmeanCurR;
		ymeanLastR=ymeanCurR;
		lastAreaB_L=curAreaB_L;
		lastAreaB_R=curAreaB_R;

		imshow("WinR",frameR);
		waitKey(10);
		imshow("WinB",frameB);
		waitKey(10);
	}
	return 0;
}