#include <iostream>
using namespace std;
#include "opencv2\core\core.hpp"
#include "opencv2\highgui\highgui.hpp"
#include "opencv2\imgproc\imgproc.hpp"
using namespace cv;
#include <stdio.h>
#include <stdlib.h>


void trackbarfunc(int x,void* data)
{
	*(double*)data = x;
}

int main(int argc, char** argv)
{
	namedWindow("Win",CV_WINDOW_AUTOSIZE);
	Mat frame,im,color;
	VideoCapture cam(0);
	vector<Mat> allMat;
	double thrR=0,maxR=0,thrG=0,maxG=0,thrB=0,maxB=0;

	namedWindow("Track",CV_WINDOW_AUTOSIZE);
	createTrackbar("Red Thresh","Track",NULL,255,trackbarfunc,&thrR);
	createTrackbar("Red Max","Track",NULL,255,trackbarfunc,&maxR);
	createTrackbar("Blue Thresh","Track",NULL,255,trackbarfunc,&thrB);
	createTrackbar("Blue Max","Track",NULL,255,trackbarfunc,&maxB);
	createTrackbar("Green Thresh","Track",NULL,255,trackbarfunc,&thrG);
	createTrackbar("Green Max","Track",NULL,255,trackbarfunc,&maxG);

	while(1)
	{   cam.read(frame);
		split(frame,allMat);
		threshold(allMat[0],allMat[0], thrB,maxB,THRESH_BINARY);
		threshold(allMat[1],allMat[1], thrG,maxG,THRESH_BINARY);
		threshold(allMat[2],allMat[2], thrR,maxR,THRESH_BINARY);
		merge(allMat,color);
		Laplacian(color,color,color.depth());
		imshow("Win",color);
		waitKey(33);
	}
	return 0;
}