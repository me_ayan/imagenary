#include <iostream>
using namespace std;
#include "opencv2\core\core.hpp"
#include "opencv2\highgui\highgui.hpp"
#include "opencv2\imgproc\imgproc.hpp"
using namespace cv;
#include <stdio.h>
#include <stdlib.h>


void trackbarfunc(int x,void* data)
{
	*(double*)data = x;
}

int main(int argc, char** argv)
{
	namedWindow("Win",CV_WINDOW_AUTOSIZE);
	Mat allMat[3];
	Mat oneMat;
	Mat frame;
	VideoCapture cam(0);
	vector<vector<Point>> conts;
	vector<Vec4i> hierarchy;
	Mat im;// = imread(cam.read(frame),CV_LOAD_IMAGE_COLOR);
	unsigned int a;
	double thr = 0;
	double maxval = 0;
	namedWindow("Track",CV_WINDOW_AUTOSIZE);
	createTrackbar("thresh","Track",NULL,255,trackbarfunc,&thr);
	createTrackbar("max","Track",NULL,255,trackbarfunc,&maxval);

	


	
	while(1)
	{   cam.read(frame);
		split(frame,allMat);
		threshold(allMat[2],oneMat, thr,maxval,THRESH_BINARY);
		//Canny( oneMat, oneMat, 100, 250, 3 );
		Laplacian(oneMat,oneMat,oneMat.depth());
		/*findContours(oneMat,conts,hierarchy,CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
		 Mat drawing = Mat::zeros( oneMat.size(), CV_8UC3 );
	for( int i = 0; i< conts.size(); i++ )
		 {
		  Scalar color = Scalar( 0, 0,0 );
		  drawContours( drawing, conts, i, color,8, 8, hierarchy,0,  Point(0, 0) );
		 }*/

		imshow("Win",oneMat);
		waitKey(33);
	}
	return 0;
}