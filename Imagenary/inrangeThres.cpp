#include <iostream>
using namespace std;
#include "opencv2\core\core.hpp"
#include "opencv2\highgui\highgui.hpp"
#include "opencv2\imgproc\imgproc.hpp"
using namespace cv;

void thresFunction(int x,void* data)
{
	*(int*)data = x;
}

int main(int argc, char** argv)
{
	VideoCapture cam(0);
	Mat frame;
	Mat thePatch = imread("patch.tif");
	namedWindow("Win"); namedWindow("TrackWindow");
	CV_Assert(cam.isOpened());

	bool isHSV = true;


	/* Forget this part */
	do
	{cam.read(frame);
	}while(frame.empty());
	/* Forget this part */


	int RL=0,RH=0,GL=0,GH=0,BL=0,BH=0;

	int HL=0,HH=0,SL=0,SH=0,VL=0,VH=0;

	if(!isHSV)
	{
		createTrackbar("R_HIGH","TrackWindow",NULL,255,thresFunction,&RH);
		createTrackbar("R_LOW","TrackWindow",NULL,255,thresFunction,&RL);
		createTrackbar("G_HIGH","TrackWindow",NULL,255,thresFunction,&GH);
		createTrackbar("G_LOW","TrackWindow",NULL,255,thresFunction,&GH);
		createTrackbar("B_HIGH","TrackWindow",NULL,255,thresFunction,&BH);
		createTrackbar("B_LOW","TrackWindow",NULL,255,thresFunction,&BL);
	} else
	{
		createTrackbar("H_HIGH","TrackWindow",NULL,255,thresFunction,&HH);
		createTrackbar("H_LOW","TrackWindow",NULL,255,thresFunction,&HL);
		createTrackbar("S_HIGH","TrackWindow",NULL,255,thresFunction,&SH);
		createTrackbar("S_LOW","TrackWindow",NULL,255,thresFunction,&SH);
		createTrackbar("V_HIGH","TrackWindow",NULL,255,thresFunction,&VH);
		createTrackbar("V_LOW","TrackWindow",NULL,255,thresFunction,&VL);
	}

	while(true)
	{
		cam.read(frame);
		
		if(isHSV)
		{
			cvtColor(frame,frame,CV_BGR2HSV);
			inRange(frame,Scalar(HL,SL,VL),Scalar(HH,SH,VH),frame);
		}
		else
			inRange(frame,Scalar(CV_RGB(RL,GL,BL)),Scalar(CV_RGB(RH,GH,BH)),frame);
		

		//matchTemplate(frame,thePatch,frame,CV_TM_SQDIFF_NORMED);

		imshow("Win",frame);
		if(waitKey(10)==27/*ESCAPE KEY*/)
			break;
	}
	return 0;
}