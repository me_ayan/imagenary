#include <iostream>
using namespace std;
#include "opencv2\core\core.hpp"
#include "opencv2\highgui\highgui.hpp"
#include "opencv2\imgproc\imgproc.hpp"
using namespace cv;
#include<Windows.h>

POINT t1;
int thresh=10;
int clickThresh=0;
bool nomove=true;
int mouseon=0;

void thresFunction(int x,void* data)
{
	*(int*)data = x;
}

void LeftClick ( )
{  
  INPUT Input={0};
  // left down 
  Input.type= INPUT_MOUSE;
  Input.mi.dwFlags = MOUSEEVENTF_LEFTDOWN;
  ::SendInput(1,&Input,sizeof(INPUT));
  // left up
  ::ZeroMemory(&Input,sizeof(INPUT));
  Input.type      = INPUT_MOUSE;
  Input.mi.dwFlags  = MOUSEEVENTF_LEFTUP;
  ::SendInput(1,&Input,sizeof(INPUT));
}


void detectClick(double curArea, double lastArea)
{
	if((curArea-lastArea)>clickThresh)
	{	
		cout<<"Click!!! "<<endl;
		if (mouseon) 
			LeftClick();
	}

}
void leftX()
{   
	nomove=false;
	cout << "Left" <<endl;
	if(mouseon) SetCursorPos(t1.x-mouseon,t1.y);
}

void rightX()
{   
	nomove=false;
	cout << "Right" <<endl;
	if(mouseon) SetCursorPos(t1.x+mouseon,t1.y);
}

void downY()
{   nomove=false;
	cout << "Up" <<endl;
	if(mouseon) SetCursorPos(t1.x,t1.y+mouseon);
}

void upY()
{   
	nomove=false;
	cout << "Down" <<endl;
	if(mouseon) SetCursorPos(t1.x,t1.y-mouseon);
}

void checkmotion(double curValX,double curValY, double lastValX,double lastValY)
{
	nomove=true;
	GetCursorPos(&t1);
	if(curValX>lastValX+thresh)
		 {rightX();}
	if(curValY>lastValY+thresh)
		{upY();}
	if(curValX<lastValX-thresh)
		{leftX();}
	if(curValY<lastValY-thresh)
		{downY();}
	if(nomove)
		cout<< "No mov" <<endl;
}


int main(int argc, char** argv)
{   
	VideoCapture cam(0);
	Moments center;
	Mat frame;
	double xmeanCur,ymeanCur,xmeanLast=0,ymeanLast=0;
	double curArea,lastArea=0;
	namedWindow("Win"); namedWindow("TrackWindow"); namedWindow("TrackWindow2");
	CV_Assert(cam.isOpened());
	
	cam.set(CV_CAP_PROP_FRAME_WIDTH,752);
	cam.set(CV_CAP_PROP_FRAME_HEIGHT,416);

	
	/* Forget this part */
	do
	{cam.read(frame);
	}while(frame.empty());
	/* Forget this part */


	int RL=0,RH=0,GL=0,GH=0,BL=0,BH=0;
	int RL2=0,RH2=0,GL2=0,GH2=0,BL2=0,BH2=0;

	createTrackbar("pxThresh","TrackWindow2",NULL,255,thresFunction,&thresh);
	createTrackbar("sizeThresh","TrackWindow2",NULL,2000,thresFunction,&clickThresh);
	createTrackbar("MouseMove","TrackWindow2",NULL,100,thresFunction,&mouseon);

	createTrackbar("R_HIGH","TrackWindow",&RH,255,thresFunction,&RH);
	createTrackbar("R_LOW","TrackWindow",&RL,255,thresFunction,&RL);
	createTrackbar("G_HIGH","TrackWindow",&GH,255,thresFunction,&GH);
	createTrackbar("G_LOW","TrackWindow",&GL,255,thresFunction,&GL);
	createTrackbar("B_HIGH","TrackWindow",&BH,255,thresFunction,&BH);
	createTrackbar("B_LOW","TrackWindow",&BL,255,thresFunction,&BL);

	while(true)
	{
		cam.read(frame);
		GaussianBlur(frame,frame,Size(11,11),2);
		inRange(frame,Scalar(CV_RGB(RL,GL,BL)),Scalar(CV_RGB(RH,GH,BH)),frame);
		dilate(frame,frame,getStructuringElement(MORPH_ELLIPSE,Size(4,4)));
		center = moments(frame,true);
		xmeanCur=center.m10/center.m00;
		ymeanCur=center.m01/center.m00;
		curArea=center.m00;
		checkmotion(xmeanCur,ymeanCur,xmeanLast,ymeanLast);
		detectClick(curArea,lastArea);
		xmeanLast=xmeanCur;
		ymeanLast=ymeanCur;
		lastArea=curArea;
		imshow("Win",frame);
		if(waitKey(10)==27/*ESCAPE KEY*/)
			return 0;
	}
	return 0;
}