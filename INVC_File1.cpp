#include "opencv2\core\core.hpp"
#include "opencv2\highgui\highgui.hpp"
#include "opencv2\imgproc\imgproc.hpp"
using namespace cv;
#include <iostream>
#include <thread>
#include <cstdlib>
using namespace std;

#define IMAGE_HEIGHT 640
#define IMAGE_WIDTH 960

void callbackfunc( int x, void *data )
{
	*(int*)data = x;
}

enum Color {RED, GREEN, BLUE, YELLOW, GRAY};
// col[0-3] > four balls on right, col[4] > left ball, col[5] > bonus ball
int col[6]={Color::RED,Color::BLUE,Color::GREEN,Color::YELLOW,Color::RED,Color::GRAY};
int bonusTop=100,bonusLeft=200;
Mat image = Mat(IMAGE_HEIGHT, IMAGE_WIDTH, CV_8UC3,Scalar::all(0));

void createCircle(float x, float y, int i,float i2,float radius, Mat& img)
{
	if(col[i]==Color::BLUE)
		circle(img, Point2f(x, y+i2*120), radius, Scalar(255,0,0),CV_FILLED);
	else if(col[i]==Color::GREEN)
		circle(img, Point2f(x, y+i2*120), radius, Scalar(0,255,0),CV_FILLED);
	else if(col[i]==Color::RED)
		circle(img, Point2f(x, y+i2*120), radius, Scalar(0,0,255),CV_FILLED);
	else if(col[i]==Color::YELLOW)
		circle(img, Point2f(x, y+i2*120), radius, Scalar(0,255,255),CV_FILLED);
	else if(col[i]==Color::GRAY)
		circle(img, Point2f(x, y+i2*120), radius, Scalar(120,120,120),CV_FILLED);
}

void changeColor()
{
	while(true)
	{
		_sleep(300);
		int i = rand()%4;
		int j= rand()%4;
		int temp;
		temp=col[i];
		col[i]=col[j];
		col[j]=temp;
	}
}

void changeColorMain()
{
	while(true)
	{
		_sleep(1000);
		col[4] = rand()%4;
	}
}


void moveBonusBall()
{
	while(true)
	{
		_sleep(1000);
		bonusTop=((rand()%470)+10);
		bonusLeft=190+(rand()%600);
	}
}


int main()
{
	namedWindow("win");
	thread t1(changeColor), t2(changeColorMain),t3(moveBonusBall);
	while (true)
	{	
		image=Mat(IMAGE_HEIGHT, IMAGE_WIDTH, CV_8UC3,Scalar::all(0));	//left circle
		createCircle(70,320,4,0,40,image);	//bonus circle
		createCircle(bonusLeft,bonusTop,5,0,20,image);

		for (int i=0;i<4;i++)	//right 4 circles
			createCircle(910-20*((1.5-i)*(1.5-i)),70,i,i*1.4,35,image);

		imshow("win", image);
		if(waitKey(1)==27)
		{
			t1.detach();t2.detach();t3.detach();
			break;
		}
	}
}

/*
More no of switches decrease points(decide on how many switches)
Overall time, maximum submissions
No passing over the 4 color blocks(disqualified or huge negative)
Bot leaves arena negative
consider obstacles- decrease points

bonus- deactivated for some time after getting one



*/