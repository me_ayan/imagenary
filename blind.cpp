#include <iostream>
using namespace std;
#include "opencv2\core\core.hpp"
#include "opencv2\highgui\highgui.hpp"
#include "opencv2\imgproc\imgproc.hpp"
#include "opencv2\features2d\features2d.hpp"
#include "opencv2\nonfree\nonfree.hpp"
#include "opencv2\nonfree\ocl.hpp"
using namespace cv;
#include "opencv2\ocl\matrix_operations.hpp"
#include "opencv2\ocl\ocl.hpp"
using namespace ocl;

void callbackFunc(int x, void* data)
{
	*(int*)data = x;
}

int main(int argc,char** argv)
{
	VideoCapture cam(0);
	CV_Assert(cam.isOpened());
	Mat frame;

	oclMat ocl_src, ocl_dst;

	do
	{
		cam >> frame;
	}while(frame.empty());

	namedWindow("View");
	namedWindow("KeyP");
	char c;
	Mat frame1;
	vector<KeyPoint> keyp;

	int FAST_THRESH = 0;
	int HESSIAN = 0;

	createTrackbar("HESSIAN","View",&HESSIAN,500,callbackFunc,&HESSIAN);

	SURF surf;
	//SIFT sift;
	Mat descriptors;

	//ocl::DevicesInfo dev_info;
	//cout << ocl::getOpenCLDevices(dev_info,ocl::CVCL_DEVICE_TYPE_GPU);
	//cout << "no of device found : " << dev_info.size() << endl;
	//cout << "Detected device : " << dev_info[0]->deviceName << endl << dev_info[0]->deviceType << endl;

	while(true)
	{
		cam >> frame;
		
		//FAST(frame,keyp,FAST_THRESH);
		//drawKeypoints(frame,keyp,frame1);
		surf.hessianThreshold = HESSIAN;
		surf.detect(frame, keyp, Mat());
		//
		
		drawKeypoints(frame, keyp, frame1);
		surf.compute(frame, keyp, descriptors);
		
		
		imshow("View",frame1);
		waitKey(10);
		imshow("KeyP", descriptors);
		waitKey(10);

		//imshow("View",frame);
		c = waitKey(10);
		if(c==27)
			break;
	}
	return 0;
}
