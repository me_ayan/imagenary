#include "opencv2\core\core.hpp"
#include "opencv2\highgui\highgui.hpp"
#include "opencv2\imgproc\imgproc.hpp"
using namespace cv;
#include <fstream>
#include <iostream>
#include <sstream>
using namespace std;
#include<Windows.h>
#define maxNoOfKeys 33

namespace
{	
	Mat frame,frameBorder,frameFingers;
	vector<vector<Point>> contsBorder,contsFinger;
	vector<Vec4i> hierarchy;
	Rect rec;
	int iterations=1,ITER=20;
	int firstTime = 1;
	int RLg=248,RHg=255,GLg=211,GHg=254,BLg=42,BHg=255;
	int RLf=231,RHf=255,GLf=0,GHf=169,BLf=0,BHf=243;
	int resolutionX=800,resolutionY=448;
	float virtualWidth=0,virtualHeight=0;
	int modifierOn=0,lastModifier=0;
}

class POINTEDGE
{
public:
	int x;
	int y;
	long long int dist()
	{
		return (x*x+y*y);
	}
};

class MAPPING
{
public:
	char key[20],keycode[6];
	float posx,posy;
	int radius,modifier;
	WORD keyCode()
	{return (WORD)strtoul(keycode, NULL, 0);}
	float posX()
	{return (posx/100)*virtualWidth;}
	float posY()
	{return (posy/100)*virtualHeight;}
};

	MAPPING keyInfo[maxNoOfKeys]={0};
	POINTEDGE cornerPoints[4];
	/*
	0 -> TopLeft
	1 -> TopRight
	2 -> BottomLeft
	3 -> BottomRight
	*/

void readMapping()
{
	ifstream inFile("mapping.txt");
	if (!inFile)
	{
		cerr << "Mapping File not found!" << endl;
	}
 
  string line;
  int i=0;
  while (getline(inFile, line))
  {
	if (!i) {i=1;continue;}
    istringstream iss(line);
    iss >> keyInfo[i].key;
	iss >> keyInfo[i].keycode;
	iss >> keyInfo[i].modifier;
	iss >> keyInfo[i].posx;
	iss >> keyInfo[i].posy;
	iss >> keyInfo[i].radius;
    i++;
  }

  inFile.close();
}

void turnOffModifiers()
{
	INPUT ip;
	ip.type = INPUT_KEYBOARD;
    ip.ki.wScan = 0;
    ip.ki.time = 0;
    ip.ki.dwExtraInfo = 0;
    ip.ki.wVk = 0x10; 
	ip.ki.dwFlags = KEYEVENTF_KEYUP;
	SendInput(1, &ip, sizeof(INPUT));
	ip.ki.wVk = 0x11; 
	ip.ki.dwFlags = KEYEVENTF_KEYUP;
	SendInput(1, &ip, sizeof(INPUT));
	ip.ki.wVk = 0x12; 
	ip.ki.dwFlags = KEYEVENTF_KEYUP;
	SendInput(1, &ip, sizeof(INPUT));
}

void sendKeypress (WORD kCode, int modifier)
{  
	INPUT ip;
	ip.type = INPUT_KEYBOARD;
    ip.ki.wScan = 0; // hardware scan code for key
    ip.ki.time = 0;
    ip.ki.dwExtraInfo = 0;
 
    // Press the key
    ip.ki.wVk = kCode; // virtual-key code
    ip.ki.dwFlags = 0; // 0 for key press
    SendInput(1, &ip, sizeof(INPUT));
	Sleep(2);
	if (!modifier)
	{
		ip.ki.dwFlags = KEYEVENTF_KEYUP; // KEYEVENTF_KEYUP for key release
		SendInput(1, &ip, sizeof(INPUT));
		if(lastModifier)
			turnOffModifiers();
	}
}


void checkKey(float disth, float distv)
{
	for(int i=1;i<maxNoOfKeys;i++)
	{
		if((((disth-keyInfo[i].posX())*(disth-keyInfo[i].posX()))+((distv-keyInfo[i].posY())*(distv-keyInfo[i].posY()))) <= ((keyInfo[i].radius)*(keyInfo[i].radius)))
		{
			sendKeypress(keyInfo[i].keyCode(),keyInfo[i].modifier);
			cout<< keyInfo[i].key<<" key pressed!!!!"<<endl;
			lastModifier=keyInfo[i].modifier;
			break;
		}
	}

}

void thresFunction(int x,void* data)
{
	*(int*)data = x;
}

void createBars()
{
	createTrackbar("RHg","Ref_Thres",&RHg,255,thresFunction,&RHg);
	createTrackbar("RLg","Ref_Thres",&RLg,255,thresFunction,&RLg);
	createTrackbar("GHg","Ref_Thres",&GHg,255,thresFunction,&GHg);
	createTrackbar("GLg","Ref_Thres",&GLg,255,thresFunction,&GLg);
	createTrackbar("BHg","Ref_Thres",&BHg,255,thresFunction,&BHg);
	createTrackbar("BLg","Ref_Thres",&BLg,255,thresFunction,&BLg);

	createTrackbar("RHf","Finger_Thres",&RHf,255,thresFunction,&RHf);
	createTrackbar("RLf","Finger_Thres",&RLf,255,thresFunction,&RLf);
	createTrackbar("GHf","Finger_Thres",&GHf,255,thresFunction,&GHf);
	createTrackbar("GLf","Finger_Thres",&GLf,255,thresFunction,&GLf);
	createTrackbar("BHf","Finger_Thres",&BHf,255,thresFunction,&BHf);
	createTrackbar("BLf","Finger_Thres",&BLf,255,thresFunction,&BLf);

	createTrackbar("FirstTime","Extra",&firstTime,1,thresFunction,&firstTime);
	createTrackbar("KeySpeed","Extra",&ITER,1,thresFunction,&ITER);
}

void detectEdges(float x,float y,int fingNo)
{
	float ah,bh,ch,av,bv,cv,disth,distv;

	ah=cornerPoints[1].x-cornerPoints[0].x;
	bh=cornerPoints[0].y-cornerPoints[1].y;
	ch=(((-bh)*(cornerPoints[0].x))-((ah)*(cornerPoints[0].y)));
	disth=(abs(ah*x+bh*y+ch))/(sqrt(ah*ah+bh*bh));

	av=cornerPoints[2].x-cornerPoints[0].x;
	bv=cornerPoints[0].y-cornerPoints[2].y;
	cv=(((-bv)*(cornerPoints[0].x))-((av)*(cornerPoints[0].y)));
	distv=(abs(av*x+bv*y+cv))/(sqrt(av*av+bv*bv));

	cout<< "Finger Pos "<<fingNo<<": "<<disth << " " <<distv<<endl;

	checkKey(disth,distv);
}

void checkCornerPoints(POINTEDGE* p)
{
	POINTEDGE temp;
	for (int c = 0 ; c < ( 4 - 1 ); c++)
	{
    for (int d = 0 ; d < 4 - c - 1; d++)
		{
		  if (p[d].dist() > p[d+1].dist())
		  {
		    temp   = p[d];
		    p[d]   = p[d+1];
		    p[d+1] = temp;
		  }
		}
	}

	virtualWidth=sqrt(((p[0].x-p[2].x)*(p[0].x-p[2].x))+((p[0].y-p[2].y)*(p[0].y-p[2].y)));
	virtualHeight=sqrt(((p[0].x-p[1].x)*(p[0].x-p[1].x))+((p[0].y-p[1].y)*(p[0].y-p[1].y)));
}

int main(int argc,char** argv)
{
	int i=0;
	cout<<"Reading Camera..."<<endl;
	
	VideoCapture cam(0);
	CV_Assert(cam.isOpened());

	cam.set(CV_CAP_PROP_FRAME_WIDTH,resolutionX);
	cam.set(CV_CAP_PROP_FRAME_HEIGHT,resolutionY);

	do
	{
		cam >> frame;
	}while(frame.empty());

	cout<<"Done"<<endl;

	cout<<"Creating Windows..."<<endl;
	namedWindow("Keyboard");
	namedWindow("Ref_Thres");
	namedWindow("Finger_Thres");
	namedWindow("Extra");
	namedWindow("Fingering");
	cout<<"Done"<<endl;

	cout<<"Creating Trackbars..."<<endl;
	createBars();
	cout<<"Done"<<endl;

	cout<<"Reading Mapping..."<<endl;
	readMapping();
	cout<<"Done"<<endl;

	cout<<"Starting Camera Feed..."<<endl;

	while(true)
	{
		cam >> frame;
		GaussianBlur(frame,frame,Size(5,5),1);
		if(firstTime)
		{
			iterations=1;
			inRange(frame,Scalar(CV_RGB(RLg,GLg,BLg)),Scalar(CV_RGB(RHg,GHg,BHg)),frameBorder);
			morphologyEx(frameBorder,frameBorder,MORPH_CLOSE,getStructuringElement(MORPH_ELLIPSE,Size(5,5)));
			dilate(frameBorder,frameBorder,getStructuringElement(MORPH_ELLIPSE,Size(5,5)));
			findContours(frameBorder,contsBorder,hierarchy,CV_RETR_EXTERNAL ,CV_CHAIN_APPROX_NONE );

			imshow("Keyboard",frameBorder);
			waitKey(10);

			if(contsBorder.size()!=4)
			{	
				cout << "ALign Keyboard " << contsBorder.size() <<  endl;
				continue;
			}
			else
				cout<< "Keyboard OK" << " " << contsBorder.size() << endl;
	
			for (i=0;i<contsBorder.size();i++)
			{
				drawContours( frameBorder, contsBorder,i, Scalar(255), 2, 8, hierarchy,0, Point() );
				rec = boundingRect(contsBorder[i]);
				rectangle(frameBorder,rec,Scalar(255));
				cornerPoints[i].x = rec.x;
				cornerPoints[i].y = rec.y;
			}

			checkCornerPoints(cornerPoints);

			imshow("Keyboard",frameBorder);
			waitKey(10);
			continue;
		}

		inRange(frame,Scalar(CV_RGB(RLf,GLf,BLf)),Scalar(CV_RGB(RHf,GHf,BHf)),frameFingers);
		morphologyEx(frameFingers,frameFingers,MORPH_CLOSE,getStructuringElement(MORPH_ELLIPSE,Size(5,5)));
		dilate(frameFingers,frameFingers,getStructuringElement(MORPH_ELLIPSE,Size(5,5)));
		findContours(frameFingers,contsFinger,hierarchy,CV_RETR_EXTERNAL,CV_CHAIN_APPROX_NONE );

		//draw circle positions
		for(i=1;i<maxNoOfKeys;i++)
			circle(frameFingers,Point2i(keyInfo[i].posX(),keyInfo[i].posY()),keyInfo[i].radius,Scalar(255));


		for (i=0;i<contsFinger.size();i++)
		{
			rec = boundingRect(contsFinger[i]);
			rectangle(frameFingers,rec,Scalar(255));
			if(iterations==ITER)
				detectEdges(rec.y+rec.height/2.0,rec.x+rec.width/2.0,i);
		}

		if(iterations==ITER)
				iterations=1;
		iterations++;

		imshow("Fingering",frameFingers);
		waitKey(10);
	}

return 0;
}